export class Employee {
  static forEach: any;
  constructor(
    public id: number,
    public firstName: string,
    public lastName: string,
    public email: string,
  ) {}
}
