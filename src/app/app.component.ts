import { Component } from '@angular/core';
import { Employee } from './models/employee';
import { RegistrationService } from './services/registration.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'angular-exam';

  constructor(private _registrationService: RegistrationService) {}

}
