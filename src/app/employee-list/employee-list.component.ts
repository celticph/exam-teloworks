import { Component, OnInit } from '@angular/core';
import * as employeesData from '../data/employee-data.json'
import { Employee } from '../models/employee';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.sass']
})
export class EmployeeListComponent implements OnInit {

  employees: Employee[] = [
    {
      "id": 456,
      "firstName": "Grace",
      "lastName": "Reyes",
      "email": "g@yahoo.com"
    },
    {
      "id": 123,
      "firstName": "Mark",
      "lastName": "Cruz",
      "email": "m@yahoo.com"
    },
    {
      "id": 343,
      "firstName": "Meredith",
      "lastName": "Gomez",
      "email": "mg@yahoo.com"
    },

  ];

  constructor() { }

  ngOnInit() {
  }

}
