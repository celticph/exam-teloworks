import { Component, OnInit } from '@angular/core';
import { Employee } from '../models/employee';
import { RegistrationService } from '../services/registration.service';

@Component({
  selector: 'app-add-new-employee',
  templateUrl: './add-new-employee.component.html',
  styleUrls: ['./add-new-employee.component.sass']
})
export class AddNewEmployeeComponent implements OnInit {

  employeeModel = new Employee(123,"John", "David", "a@a.com");

  submitted = false;

  constructor(private _registrationService: RegistrationService) { }

  ngOnInit() {
  }

  onSubmit(employeeForm: any) {
    console.log(employeeForm);
    this.submitted = true;
    this._registrationService.register(this.employeeModel)
    .subscribe(
      data => console.log('Success', data),
      error => console.error('Error', error)
    )
  }

}
