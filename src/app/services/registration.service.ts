import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/employee';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  _url = 'http://localhost:3000/register';

  constructor(
    private _http: HttpClient
  ) { }

  register(user: Employee) {
    return this._http.post<any>(this._url, user)
  }
}
